const RESULT = {
  resolve: "Promise fulfilled",
  reject: "reject rejected",
};

const callPromise = () =>
  new Promise((resolve, reject) => {
    const success = Math.floor(Math.random() * 200) + 1 > 100;

    setTimeout(() => {
      if (success) {
        resolve(RESULT.resolve);
      } else {
        reject(new Error(RESULT.reject));
      }
    }, 900);
  });

const task1 = () =>
  callPromise()
    .then((result) => console.log(result))         // Можно еще было сделать через .then(value=> console.log(value), reason => alert("Произошла ошибка!")) без чейна catch;
    .catch((error) => alert("Произошла ошибка!")); // Так как Метод then() возвращает Promise. Метод принимать два аргумента: колбэк-функции для случаев выполнения и отклонения промиса.

task1();

const task2 = async () => {
  try {
    const resultFunc = await callPromise();
    console.log(resultFunc);
  } catch (error) {
    alert("Произошла ошибка!");
  }
};
task2();