const { myForm } = document.forms;

const hiddenInput = myForm.querySelector('input[name="hiddenInput"]');

const successMessage = (parentNode, message) => {
  let newMessage = document.createElement("span");
  newMessage.textContent = message;
  parentNode.appendChild(newMessage);
};

const sendDataRequest = (form, name, value) => {
  let data = new FormData(form);

  if (!form.querySelector(`input[name="${name}"]`)) data.append(name, value);

  const URL = form.getAttribute("action");

  fetch(URL, {
    body: data,
    method: "POST",
  }).then((response) => {
    const msg = response.ok ? "Форма отправлена!" : "Ошибка сервера!";
    successMessage(form, msg);
  });
};

const handleSubmit = (e) => {
  e.preventDefault();
  if (hiddenInput) hiddenInput.remove();
  sendDataRequest(myForm, "newField", "customValue");
};

myForm.addEventListener("submit", handleSubmit);
